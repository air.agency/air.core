<?php

namespace Air\Core\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use \DateTime;

trait TimeFromTrait
{
    /**
     * Time from H:i
     *
     * @var \DateTime
     *
     * @Serializer\Type("DateTime<'H:i'>")
     * @Serializer\Groups({"list", "view"})
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(name="time_from", type="time", nullable=false)
     */
    public ?DateTime $timeFrom = null;

    public function getTimeFrom():?DateTime
    {
        return $this->timeFrom;
    }

    public function setTimeFrom(?DateTime $timeFrom)
    {
        $this->timeFrom = $timeFrom;
        return $this;
    }
}
