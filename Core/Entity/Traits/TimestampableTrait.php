<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use DateTime;

trait TimestampableTrait
{
    /**
     * Created at
     *
     * @var \DateTime
     *
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     * @Serializer\Groups({"view"})
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default": "CURRENT_TIMESTAMP"})
     */ 
    public $createdAt;

    /**
     * Updated at
     *
     * @var \DateTime
     *
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     * @Serializer\Groups({"view"})
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default": "CURRENT_TIMESTAMP"})
     */
    public $updatedAt;

    /**
     * Deleted at
     *
     * @var \DateTime
     *
     * @ORM\Column(name="system_deleted_at", type="datetime", nullable=true)
     */
    public $systemDeletedAt;


    /**
     * @return \DateTime
     */
    public function getSystemDeletedAt():?DateTime
    {
        return $this->systemDeletedAt;
    }

    /**
     * @return self
     */
    public function setSystemDeletedAt(?DateTime $systemDeletedAt)
    {
        $this->systemDeletedAt = $systemDeletedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt():?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt():?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return \DateTime
     */
    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->updatedAt = new DateTime();
        if (!$this->createdAt) {
            $this->createdAt = new DateTime();
        }
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onPreUpdate()
    {
        if (!$this->createdAt) {
            $this->createdAt = new DateTime();
        }
        $this->updatedAt = new DateTime();
    }
}
