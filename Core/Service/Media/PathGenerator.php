<?php
namespace Air\Core\Service\Media;

use Sonata\MediaBundle\Provider\ImageProvider;
use Air\Application\Sonata\MediaBundle\Entity\Media;

class PathGenerator
{
    public function __construct(ImageProvider $provider)
    {
        $this->provider = $provider;
    }

    public function getPath($media = null, $format = 'reference')
    {
        if (!$media) { 
            return '';
        }

        $format = $this->provider->getFormatName($media, $format);

        return $this->provider->generatePublicUrl($media, $format);
    }
}
