<?php

namespace Air\Core\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use \DateTime;

trait PublishedAtTrait
{
    /**
     * Published At
     *
     * @Serializer\Type("datetime")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Column(name="published_at", type="datetime", nullable=true)
     */
    protected ?DateTime $publishedAt = null;

    /**
     * @param DateTime $publishedAt
     */
    public function setPublishedAt(?DateTime $publishedAt)
    {
        $this->publishedAt = $publishedAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getPublishedAt():?DateTime
    {
        return $this->publishedAt;
    }
}
