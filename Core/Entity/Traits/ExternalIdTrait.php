<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait ExternalIdTrait
{
    /**
     * External id
     *
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list", "view"})
     *
     * @Assert\Length(max=512)
     *
     * @ORM\Column(name="external_id", type="string", length=512, nullable=false)
     */
    protected ?string $externalId = null;

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param string|null $externalId
     *
     * @return self
     */
    public function setExternalId(?string $externalId): self
    {
        $this->externalId = $externalId;

        return $this;
    }
}
