<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait CodeTrait
{
    /**
     * Code
     *
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list", "view"})
     *
     * @Assert\NotBlank
     * @Assert\Length(max=512)
     *
     * @ORM\Column(name="code", type="string", length=512, nullable=false)
     */
    protected ?string $code = null;

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     *
     * @return self
     */
    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
