<?php

namespace Air\Core\Entity\Media\Traits;

use App\Application\Sonata\MediaBundle\Entity\Media;
use JMS\Serializer\Annotation as Serializer;

trait FileTrait
{
    /**
     * @var Media
     *
     *
     * @Serializer\Type("App\Application\Sonata\MediaBundle\Entity\Media")
     * @Serializer\Groups({"list", "view"})
     * @Serializer\MaxDepth(2)
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="file", referencedColumnName="id")
     * })
     */
    private $file;

    /**
     * Set file
     *
     * @param Media $media
     * @return $this
     */
    public function setFile(?Media $media = null)
    {
        $this->file = $media;

        return $this;
    }

    /**
     * Get file
     *
     * @return Media
     */
    public function getFile()
    {
        return $this->file;
    }
}
