<?php
namespace Air\Core\Entity;

use Doctrine\ORM\Mapping as ORM;

use Air\Core\Entity\Traits;
use Air\Core\Entity\Media\Traits as MediaTraits;
use Air\Core\Entity\AbstractEntity;

abstract class AbstractPicture extends AbstractEntity
{
    //use Traits\IdTrait;
    use Traits\PublishedTrait;
    //use Traits\TimestampableTrait;
    //use Traits\AuthorAwareTrait;
    use Traits\OrderingTrait;
    use Traits\DescriptionTrait;
    use MediaTraits\PictureTrait;
}
