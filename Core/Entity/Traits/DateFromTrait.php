<?php

namespace Air\Core\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use \DateTime;

trait DateFromTrait
{
    /**
     * Date from
     *
     * @Serializer\Type("datetime")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Column(name="date_from", type="datetime", nullable=true)
     */
    protected ?DateTime $dateFrom = null;

    /**
     * @param DateTime $dateFrom
     */
    public function setDateFrom(?DateTime $dateFrom)
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateFrom():?DateTime
    {
        return $this->dateFrom;
    }
}
