<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;

trait SeoTrait
{
    /**
     * Seo No Index
     *
     * @var string|null
     *
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"view"})
     *
     * @ORM\Column(name="seo_no_index", type="boolean", nullable=false, options={"default" : false}) 
     */
    protected ?bool $seoNoIndex = false;

    /**
     * Seo h1
     *
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"view"})
     *
     * @Gedmo\Translatable
     *
     * @ORM\Column(name="seo_h1", type="text", nullable=true)
     */
    protected ?string $seoH1 = null;

    /**
     * Seo title
     *
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"view"})
     *
     * @Gedmo\Translatable
     *
     * @ORM\Column(name="seo_title", type="text", nullable=true)
     */
    protected ?string $seoTitle = null;

    /**
     * Seo description
     *
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"view"})
     *
     * @Gedmo\Translatable
     *
     * @ORM\Column(name="seo_description", type="text", nullable=true)
     */
    protected ?string $seoDescription = null;

    /**
     * Seo keywords
     *
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"view"})
     *
     * @Gedmo\Translatable
     *
     * @ORM\Column(name="seo_keywords",type="text", nullable=true)
     */
    protected ?string $seoKeywords = null;

    /**
     * @return bool|null
     */
    public function getSeoNoIndex(): ?bool
    {
        return $this->seoNoIndex;
    }

    /**
     * @param bool|null $seoNoIndex
     *
     * @return self
     */
    public function setSeoNoIndex(?bool $seoNoIndex): self
    {
        $this->seoNoIndex = $seoNoIndex;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSeoH1(): ?string
    {
        return $this->seoH1;
    }

    /**
     * @param string|null $seoH1
     *
     * @return self
     */
    public function setSeoH1(?string $seoH1): self
    {
        $this->seoH1 = $seoH1;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSeoTitle(): ?string
    {
        return $this->seoTitle;
    }

    /**
     * @param string|null $seoTitle
     *
     * @return self
     */
    public function setSeoTitle(?string $seoTitle): self
    {
        $this->seoTitle = $seoTitle;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSeoKeywords(): ?string
    {
        return $this->seoKeywords;
    }

    /**
     * @param string|null $seoTitle
     *
     * @return self
     */
    public function setSeoKeywords(?string $seoKeywords): self
    {
        $this->seoKeywords = $seoKeywords;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSeoDescription(): ?string
    {
        return $this->seoDescription;
    }

    /**
     * @param string|null $seoDescription
     *
     * @return self
     */
    public function setSeoDescription(?string $seoDescription): self
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }
}
