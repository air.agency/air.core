<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;

trait AnnounceTrait
{
    /**
     * Announce
     *
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"view", "list"})
     *
     * @Gedmo\Translatable
     *
     * @ORM\Column(name="announce", type="text", nullable=true)
     *
     */
    protected ?string $announce = null;

    /**
     * @param string|null $announce
     */
    public function setAnnounce(?string $announce):self
    {
        $this->announce = $announce;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAnnounce():?string
    {
        return $this->announce;
    }
}
