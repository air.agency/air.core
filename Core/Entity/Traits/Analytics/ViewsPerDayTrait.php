<?php

namespace Air\Core\Entity\Traits\Analytics;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait ViewsPerDayTrait
{
    /**
     * Analytics Views Per Day
     *
     * @var string
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Column(name="analytics_views_per_day", type="integer", nullable=true)
     */
    protected ?int $analyticsViewsPerDay = null;

    /**
     * @return null|int
     */
    public function getAnalyticsViewsPerDay():?int
    {
        return $this->analyticsViewsPerDay;
    }

    /**
     * @param null|int $analyticsViewsPerDay
     * @return self
     */
    public function setAnalyticsViewsPerDay(?int $analyticsViewsPerDay): self
    {
        $this->analyticsViewsPerDay = $analyticsViewsPerDay;
        return $this;
    }
}
