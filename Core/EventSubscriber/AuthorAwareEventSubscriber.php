<?php

namespace Air\Core\EventSubscriber;

use App\User\Entity\User;
use Air\Core\Entity\AuthorAwareInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AuthorAwareEventSubscriber implements EventSubscriber
{
    /** @var User|null */
    private $tokenStorage;

    /**SerializerHelper
     * AuthorAwareEventSubscriber constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function getUser(): ?User
    {
        $token = $this->tokenStorage->getToken();
        if ($token === null) {
            return null;
        }

        if ($token->getUser() instanceof User) {
            return $token->getUser();
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();

        if ($entity instanceof AuthorAwareInterface) {
            $user = $this->getUser();
            $entity->setCreatedBy($user);
            $entity->setUpdatedBy($user);
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();

        if ($entity instanceof AuthorAwareInterface) {
            $user = $this->getUser();
            $entity->setUpdatedBy($user);
        }
    }
}
