<?php

namespace Air\Core\Service\Serializer;

use App\Application\Sonata\MediaBundle\Entity\Gallery;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\Metadata\StaticPropertyMetadata;
use Air\Core\Service\Media\PathGenerator;

class SerializerGalleryHandler
{
    /** @var PathGenerator|null */
    private $pathGenerator = null;

    public function __construct(PathGenerator $pathGenerator)
    {
        $this->pathGenerator = $pathGenerator;
    }

    public function getPath(Media $media = null, $format = 'reference')
    {
        if (!$media) {
            return '';
        }

        $format = $this->provider->getFormatName($media, $format);

        return $this->provider->generatePublicUrl($media, $format);
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        /** @var MySpecialObjectType $object */
        $object = $event->getObject();

        if (is_a($object, Gallery::class)) {
            $data = [];
            foreach ($object->getGalleryItems() as $galleryItem) {
                $media = $galleryItem->getMedia();
                if (!$media) {
					continue;
				}
                
                if ($object->getContext() == 'file') {
                    $reference = $this->pathGenerator->getPath($media, 'reference');
                    $data[] = [
                        'originalPath' => $reference,
                        'size' => $media->getSize(),
                        'name' => $media->getName(),
                    ];
                } else {
                    $small = $this->pathGenerator->getPath($media, 'small');
                    $medium = $this->pathGenerator->getPath($media, 'medium');
                    $big = $this->pathGenerator->getPath($media, 'reference');
                    $reference = $this->pathGenerator->getPath($media, 'reference');

                    $data[] = [
                        'smallPath' => $small,
                        'mediumPath' => $medium,
                        'bigPath' => $big,
                        'originalPath' => $reference,
                    ];
                }
            }

            $event->getVisitor()->visitProperty(
                new StaticPropertyMetadata('', "id", $object->getId()),
                $object->getId()
            );

            $event->getVisitor()->visitProperty(
                new StaticPropertyMetadata('', "title", $object->getName()),
                $object->getName()
            );

            $event->getVisitor()->visitProperty(
                new StaticPropertyMetadata('', "list", $data),
                $data
            );
        }
    }
}
