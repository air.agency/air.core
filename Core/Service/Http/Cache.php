<?php

namespace Air\Core\Service\Http;

class Cache
{
    protected $cachePath;

    public function __construct(string $cachePath)
    {
        $this->cachePath = $cachePath."/..";
    }

    public function getFileName(string $fileName):string
    {
        $fileName = str_replace('http://', '', $fileName);
        $fileName = str_replace('/', '-', $fileName);
        $fileName = str_replace('.', '-', $fileName);
        $fileName = $fileName.".html";

        return $fileName;
    }

    public function addCache(string $path, string $fileName, string $content):void
    {
        $fullPath = $this->cachePath."/".$path;

        $folders = explode("/", $path);

        $subPath = '';
        foreach($folders as $folder) {
            $subPath = $subPath."/".$folder;
            if (!file_exists($this->cachePath.$subPath)) {
                mkdir($this->cachePath.$subPath);
            }
        }

        $fileName = $this->getFileName($fileName);
        file_put_contents($fullPath."/".$fileName, $content);
    }

    public function removeCache(string $path, string $fileName):void
    {
        $fileName = $this->getFileName($fileName);
        $fullPath = $this->cachePath."/".$path."/".$fileName;
        if (file_exists($fullPath)) {
            unlink($fullPath);
        }
    }

    public function getCache(string $path, string $fileName):?string
    {
        $fileName = $this->getFileName($fileName);
        $fullPath = $this->cachePath."/".$path."/".$fileName;
        if (file_exists($fullPath)) {
            return file_get_contents($fullPath);
        }
        return null;
    }

    public function getClient(bool $withProxy = false)
    {
        if (!$this->client) {
            $config = [
                'timeout'  => self::TIMEOUT,
                'defaults' => [
                    'headers'  => ['content-type' => 'multipart/form-data'],
                ],
                'cookies' => true,
            ];

            if ($withProxy) {
                $proxy['proxy'] = 'socks5://127.0.0.1:9150';
            }

            $this->client = new \GuzzleHttp\Client($config);
        }

        return $this->client;
    }


    /**
     * @return null|string
     */
    public function getContent(string $url):?string
    {
        $res = $this->getClient()->request("GET", $url);

        $result = (string)$res->getBody();
        return $result;
    }

}
