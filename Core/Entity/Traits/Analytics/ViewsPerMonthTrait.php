<?php

namespace Air\Core\Entity\Traits\Analytics;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait ViewsPerMonthTrait
{
    /**
     * Analytics Views Per Month
     *
     * @var string
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Column(name="analytics_views_per_month", type="integer", nullable=true)
     */
    protected ?int $analyticsViewsPerMonth = null;

    /**
     * @return null|int
     */
    public function getAnalyticsViewsPerMonth():?int
    {
        return $this->analyticsViewsPerMonth;
    }

    /**
     * @param null|int $analyticsViewsPerMonth
     * @return self
     */
    public function setAnalyticsViewsPerMonth(?int $analyticsViewsPerMonth): self
    {
        $this->analyticsViewsPerMonth = $analyticsViewsPerMonth;
        return $this;
    }
}
