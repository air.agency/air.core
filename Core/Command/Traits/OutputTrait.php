<?php

namespace Air\Core\Command\Traits;

use Symfony\Component\Console\Output\OutputInterface;

trait OutputTrait
{
    protected ?OutputInterface $output = null;

    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function writeln($text)
    {
        if ($this->output) {
            $this->output->writeln($text);
        }
    }

    public function writeError($text)
    {
        $this->writeln(sprintf('Error: <error>%s<error>', $text));
    }

    public function writeInfo($text)
    {
        $this->writeln(sprintf('Info: <info>%s<info>', $text));
    }
}
