<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;


trait ContentEmptyTrait
{
    /**
     * Content
     *
     * @var string|null
     *
     * @Gedmo\Translatable
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"view"})
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    protected ?string $content = null;

    /**
     * @param string|null $content
     */
    public function setContent(?string $content):self
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent():?string
    {
        return $this->content;
    }
}
