<?php

namespace Air\Core\Security\Firewall;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

class ApiOpeneRequestMatcher implements RequestMatcherInterface
{
    /**
     * @inheritDoc
     */
    public function matches(Request $request): bool
    {
        return in_array($request->attributes->get('_route'), [
            'app.swagger',
            'app.swagger_ui',
            'api_description',
            'gesdinet_jwt_refresh_token',
            'api.security.token.refresh',
        ], true);
    }
}
