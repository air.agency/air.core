<?php

namespace Air\Core\Entity\Traits\Analytics;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait ViewsTotal
{
    /**
     * Analytics total views
     *
     * @var string
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Column(name="analytics_views_total", type="integer", nullable=true)
     */
    protected ?int $analyticsViewsTotal = null;

    /**
     * @return null|int
     */
    public function getAnalyticsViewsTotal():?int
    {
        return $this->analyticsViewsTotal;
    }

    /**
     * @param null|int $analyticsViewsTotal
     * @return self
     */
    public function setAnalyticsViewsTotal(?int $analyticsViewsTotal): self
    {
        $this->analyticsViewsTotal = $analyticsViewsTotal;
        return $this;
    }
}
