<?php

namespace Air\Core\Entity\Media\Traits;

use App\Application\Sonata\MediaBundle\Entity\Media;
use JMS\Serializer\Annotation as Serializer;

trait PictureTrait
{
    /**
     * Picture
     *
     * @var Media
     *
     * @Serializer\Type("App\Application\Sonata\MediaBundle\Entity\Media")
     * @Serializer\Groups({"list", "view"})
     * @Serializer\MaxDepth(1)
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="picture", referencedColumnName="id")
     * })
     */
    private $picture;

    /**
     * Set picture
     *
     * @param Media $media
     * @return $this
     */
    public function setPicture(?Media $media = null)
    {
        $this->picture = $media;

        return $this;
    }

    /**
     * Get picture
     *
     * @return Media|null
     */
    public function getPicture():?Media
    {
        return $this->picture;
    }
}
