<?php

namespace Air\Core\Service\Http;

use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\StaleElementReferenceException;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Chrome\ChromeOptions;

class SeleniumClient implements ClientInterface
{
    /** @var string */
    protected $seleniumHost = "http://localhost:4444/wd/hub";

    protected $seleniumClient = null;

    /**
     * @return null|string
     */
    public function getBrowser(string $url)
    {
        $browser = $this->getClient();
        $browser->get($url);

        return $browser;
    }

    /**
     * @return null|string
     */
    public function quitBrowser($browser):void
    {
        $browser->quit();
    }

    /**
     * @return null|string
     */
    public function getContent(string $url):?string
    {
        $driver = $this->getBrowser($url);
        $result = $driver->getPageSource();

       // $result = $driver->findElement(WebDriverBy::tagName('body'))->getText();
        $this->quitBrowser($driver);

        return (string) $result;
    }

    public function getClient(bool $withProxy = false)
    {
        $hide = false;
        if (!$this->seleniumHost) {
            throw new \Exception("Selenium is required");
        }

        if (!$this->seleniumClient) {
            $desiredCapabilities = DesiredCapabilities::htmlUnitWithJS();
            $chromeOptions = new ChromeOptions();

            $arguments = [
                "--user-agent=Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)",
                "start-maximized",
                'disable-infobars',
                "--disable-gpu",
                '--disable-extensions',
                '--disable-dev-shm-usage',
                '--no-sandbox'
            ];

            if ($withProxy) {
                $arguments[] = '--proxy-server=socks5://127.0.0.1:9150';
            }

            if ($hide) {
                $arguments[] = '--headless';
            }

            $chromeOptions->addArguments($arguments);

            $desiredCapabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

            $this->seleniumClient = RemoteWebDriver::create($this->seleniumHost, $desiredCapabilities, 5000);
        }

        return $this->seleniumClient;
    }
}
