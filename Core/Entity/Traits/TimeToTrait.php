<?php

namespace Air\Core\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use \DateTime;

trait TimeToTrait
{
    /**
     * Time to H:i
     *
     * @var \DateTime
     *
     * @Serializer\Type("DateTime<'H:i'>")
     * @Serializer\Groups({"list", "view"})
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(name="time_to", type="time", nullable=false)
     */
    public ?DateTime $timeTo;

    public function getTimeTo():?DateTime
    {
        return $this->timeTo;
    }

    public function setTimeTo(?DateTime $timeTo)
    {
        $this->timeTo = $timeTo;
        return $this;
    }
}
