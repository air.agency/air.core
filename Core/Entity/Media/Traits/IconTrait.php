<?php

namespace Air\Core\Entity\Media\Traits;

use App\Application\Sonata\MediaBundle\Entity\Media;
use JMS\Serializer\Annotation as Serializer;

trait IconTrait
{
    /**
     * Icon
     *
     * @var Media
     *
     * @Serializer\Type("App\Application\Sonata\MediaBundle\Entity\Media")
     * @Serializer\Groups({"list", "view"})
     * @Serializer\MaxDepth(1)
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="icon", referencedColumnName="id")
     * })
     */
    private $icon;

    /**
     * Set icon
     *
     * @param Media $media
     * @return $this
     */
    public function setIcon(?Media $media = null)
    {
        $this->icon = $media;

        return $this;
    }

    /**
     * Get icon
     *
     * @return Media|null
     */
    public function getIcon():?Media
    {
        return $this->icon;
    }
}
