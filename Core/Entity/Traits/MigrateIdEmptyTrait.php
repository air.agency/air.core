<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait MigrateIdEmptyTrait
{
    /**
     * Migrate id
     *
     * @var string|null
     *
     * @Serializer\Type("int")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Column(name="migrate_id", type="integer", nullable=true, unique=true)
     */
    protected ?int $migrateId = null;

    /**
     * @return int|null
     */
    public function getMigrateId(): ?int
    {
        return $this->migrateId;
    }

    /**
     * @param int|null $migrateId
     *
     * @return self
     */
    public function setMigrateId(?int $migrateId): self
    {
        $this->migrateId = $migrateId;

        return $this;
    }
}
