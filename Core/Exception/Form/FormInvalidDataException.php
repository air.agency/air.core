<?php

namespace Air\Core\Exception\Form;

use Air\Core\Exception\ClientException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class FormInvalidDataException extends ClientException
{
    /** @var FormInterface */
    private $form;

    /**
     * FormDataInvalidException constructor.
     *
     * @param FormInterface  $form
     * @param Throwable|null $previous
     */
    public function __construct(FormInterface $form, Throwable $previous = null)
    {
        parent::__construct('', Response::HTTP_UNPROCESSABLE_ENTITY, $previous);

        $this->form = $form;
    }

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface
    {
        return $this->form;
    }
}
