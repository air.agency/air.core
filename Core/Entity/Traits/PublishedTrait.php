<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

trait PublishedTrait
{
    /**
     * Published
     *
     * @var bool|null
     *
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Column(name="published", type="boolean", nullable=false, options={"default" : true})
     */
    protected ?bool $published = true;

    /**
     * @return bool|null
     */
    public function getPublished(): ?bool
    {
        return $this->published;
    }

    /**
     * @param bool|null $published
     *
     * @return self
     */
    public function setPublished(?bool $published): self
    {
        $this->published = $published;

        return $this;
    }
}
