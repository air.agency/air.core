<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

trait CommentsEnabledTrait
{
    /**
     * Comments enabled
     *
     * @var bool|null
     *
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Column(name="comments_enabled", type="boolean", nullable=false, options={"default" : false})
     */
    protected ?bool $commentsEnabled = false;

    /**
     * @return bool|null
     */
    public function getCommentsEnabled(): ?bool
    {
        return $this->commentsEnabled;
    }

    /**
     * @param bool|null $commentsEnabled
     *
     * @return self
     */
    public function setCommentsEnabled(?bool $commentsEnabled): self
    {
        $this->commentsEnabled = $commentsEnabled;

        return $this;
    }
}
