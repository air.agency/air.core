<?php

namespace Air\Core\Service\Http;

interface ClientInterface
{
    public function getContent(string $url): ?string;
    public function getClient(bool $withProxy);
}
