<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait IdTrait
{
    /**
     * Id
     *
     * @var integer
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Assert\Type("int")
     */
    protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
