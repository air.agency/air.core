<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

trait GuidEntityTrait
{
    /**
     * Guid
     *
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Column(name="guid", type="guid", unique=true)
     *
     */
    protected $guid;

    /**
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * @param string|null $guid
     *
     * @return self
     */
    public function setGuid(?string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }
}
