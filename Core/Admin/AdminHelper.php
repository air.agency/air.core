<?php

namespace Air\Core\Admin;

use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Sonata\AdminBundle\Form\Type\ModelListType;

class AdminHelper
{
    static public function addSeoBlock(FormMapper $formMapper)
    {
        $formMapper
            ->tab('SEO')
            ->with('SEO')
                ->add('seoH1', TextareaType::class, ['required' => false])
                ->add('seoNoIndex', CheckboxType::class, ['required' => false])
                ->add('seoTitle', TextareaType::class, ['required' => false])
                ->add('seoDescription', TextareaType::class, ['required' => false])
                ->add('seoKeywords', TextareaType::class, ['required' => false])
            ->end()
            ->end();

        return $formMapper;
    }

    static public function addAnalyticsBlock(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Analytics')
            ->with('Analytics')
                ->add('analyticsViewsPerDay', NumberType::class, ['required' => false])
                ->add('analyticsViewsPerMonth', NumberType::class, ['required' => false])
                ->add('analyticsViewsTotal', NumberType::class, ['required' => false])
            ->end()
            ->end();

        return $formMapper;
    }

    static public function addMigrateInfo(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Migration')
            ->with('Migration')
                ->add('migrateId', TextType::class, ['required' => false])
                ->add('migrateInfo', TextareaType::class, ['required' => false])
            ->end()
            ->end();

        return $formMapper;
    }

    static public function addSystemInfo(FormMapper $formMapper, $object = null)
    {

        if ($object && !$object->getId() && !$object->getCreatedAt()) {
            $object->setCreatedAt(new \DateTime());
        }
        if ($object && !$object->getId() && !$object->getUpdatedAt()) {
            $object->setUpdatedAt(new \DateTime());
        }

        $formMapper
            ->tab('System info')
            ->with('System info')
            ->add('createdAt', DateTimeType::class, [
                'data' => new \DateTime(),
                'required' => false,
                'disabled' => true
            ])
            ->add('updatedAt', DateTimeType::class, [
                'data' => new \DateTime(),
                'required' => false,
                'disabled' => true
            ])
        ;

        if (!$object || method_exists($object, 'getCreatedBy'))
        {
            $formMapper
                ->add('createdBy', ModelListType::class, [
                    'required' => false,
                    'disabled' => true,
                    'btn_edit' => false,
                    'btn_delete' => false,
                    'btn_list' => false,
                    'btn_add' => false,
                ])
            ;
        }

        if (!$object || method_exists($object, 'getCreatedBy'))
        {
            $formMapper
                ->add('updatedBy', ModelListType::class, [
                    'required' => false,
                    'disabled' => true,
                    'btn_edit' => false,
                    'btn_delete' => false,
                    'btn_list' => false,
                    'btn_add' => false,
                ])
            ;
        }

        if (!$object || method_exists($object, 'getCreatedBy'))
        {
            $formMapper
                ->add('deletedBy', ModelListType::class, [
                    'required' => false,
                    'disabled' => true,
                    'btn_edit' => false,
                    'btn_delete' => false,
                    'btn_list' => false,
                    'btn_add' => false,
                ])
            ;
        }

        $formMapper
            ->end()
            ->end()
        ;

        return $formMapper;
    }

    static public function addAuthorInfo(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Author info')
            ->with(' ')
            ->add('createdBy', ModelListType::class, ['required' => false, 'disabled' => true])
            ->add('updatedBy', ModelListType::class, ['required' => false, 'disabled' => true])
            ->add('deletedBy', ModelListType::class, ['required' => false, 'disabled' => true])
            ->end()
            ->end();

        return $formMapper;
    }

    static public function addTimestampable(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Time info')
            ->with(' ')
            ->add('createdAt', DateTimeType::class, ['required' => false, 'disabled' => true])
            ->add('updatedAt', DateTimeType::class, ['required' => false, 'disabled' => true])
            ->end()
            ->end();

        return $formMapper;
    }

    static public function getYears(): array
    {
        $date = new \DateTime();

        $years = [];
        for($i = (int) $date->format('Y'); $i > 1960 ; $i--) {
            $years[] = $i;
        }

        return $years;
    }
}
