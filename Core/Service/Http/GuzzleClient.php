<?php

namespace Air\Core\Service\Http;

use GuzzleHttp\Psr7;
use Symfony\Component\DomCrawler\Crawler;

class GuzzleClient implements ClientInterface
{
    /** @var int */
    protected const TIMEOUT = 600;

    protected $client = null;

    /** @var Cache */
    protected Cache $httpCache;

    protected array $options = [];

    public function __construct(Cache $httpCache)
    {
        $this->httpCache = $httpCache;
    }

    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    public function getClient(bool $withProxy = true): \GuzzleHttp\Client
    {
        if (!$this->client) {
            $options = [
                'timeout'  => self::TIMEOUT,
                'defaults' => [
                    'headers'  => ['content-type' => 'multipart/form-data'],
                ],
                'cookies' => true,
            ];

            if ($withProxy) {
               // $options['proxy'] = 'socks5://127.0.0.1:9150';
            }

            if ($this->options) {
                $options = $this->options;
            }
            $this->client = new \GuzzleHttp\Client($options);
        }

        return $this->client;
    }

    /**
     * @param string $url
     * @param string|null $cachePath
     * @return null|array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getHeaders(string $url):array
    {
        $response = $this->getClient()->head( $url);
        $headers = $response->getHeaders();

        return $headers;
    }

    /**
     * @param string $url
     * @return null|array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMeta(string $url):?array
    {
        $html = null;
        $code = null;
        $response = $this->getContentResponse($url);
        if ($response) {
            $html = (string)$response->getBody();
            $code = $response->getStatusCode();
        }

        if (!$html) {
            return null;
        }
        $crawler = new Crawler($html);
        $title = $crawler->filter('title')->text();
        $description = $crawler->filter('meta[name="description"]')->eq(0)->attr('content');
        $keywords = $crawler->filter('meta[name="keywords"]')->eq(0)->attr('content');

        return [
            'title'=> $title,
            'description'=> $description,
            'keywords'=> $keywords,
            'statusCode' => $code,
        ];
    }

    /**
     * @param string $url
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getContentResponse(string $url)
    {
        $response = null;
        try {
            $response = $this->getClient()->request("GET", $url);
        } catch (\Exception $exception) {
            $response = null;
        }

        return $response;
    }


    /**
     * @param string $url
     * @param string|null $cachePath
     * @return null|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getContent(string $url, ?string $cachePath = null):?string
    {
        $content = '';
        if ($cachePath) {
            $content = $this->httpCache->getCache($cachePath, $url);
        }

        if (!$content) {
            $response = $this->getContentResponse($url);
            if ($response) {
                $content = (string)$response->getBody();
            }
/*
            try {
                $response = $this->getClient()->request("GET", $url);
                $content = (string)$response->getBody();
                $code = $response->getStatusCode();
            } catch (\Exception $exception) {
                return null;
            }
*/
        }
        

        return $content;
    }

}
