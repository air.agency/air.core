<?php

namespace Air\Core\Entity\Media\Traits;

use App\Application\Sonata\MediaBundle\Entity\Media;
use JMS\Serializer\Annotation as Serializer;

trait BackgroundPictureTrait
{
    /**
     * Background picture
     *
     * @var Media
     *
     * @Serializer\Type("App\Application\Sonata\MediaBundle\Entity\Media")
     * @Serializer\Groups({"list", "view"})
     * @Serializer\MaxDepth(1)
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="background_picture", referencedColumnName="id")
     * })
     */
    private $backgroundPicture;

    /**
     * Set background picture
     *
     * @param Media $media
     * @return $this
     */
    public function setBackgroundPicture(?Media $media = null)
    {
        $this->backgroundPicture = $media;

        return $this;
    }

    /**
     * Get background picture
     *
     * @return Media|null
     */
    public function getBackgroundPicture():?Media
    {
        return $this->backgroundPicture;
    }
}
