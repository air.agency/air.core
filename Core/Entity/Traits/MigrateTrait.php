<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait MigrateTrait
{
    use MigrateIdEmptyTrait;
    use MigrateInfoTrait;
}
