<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;

trait AddressTrait
{
    /**
     * Address
     *
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list"})
     *
     * @Assert\Length(max=512)
     *
     * @Gedmo\Translatable
     *
     * @ORM\Column(name="address", type="string", length=512, nullable=true)
     */
    protected ?string $address = null;

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     *
     * @return self
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }
}
