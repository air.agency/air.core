<?php
namespace Air\Core\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Mapping\ClassMetadata;

use Air\Core\Entity\Traits;
use Air\Core\Entity\AuthorAwareInterface;

abstract class AbstractEntity implements AuthorAwareInterface
{
    use Traits\IdTrait;
    use Traits\TimestampableTrait;
    use Traits\AuthorAwareTrait;
}
