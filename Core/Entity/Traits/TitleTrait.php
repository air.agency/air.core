<?php

namespace Air\Core\Entity\Traits;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait TitleTrait
{
    /**
     * Title
     *
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list", "view"})
     *
     * @Assert\NotBlank
     * @Assert\Length(max=512)
     *
     * @Gedmo\Translatable
     *
     * @ORM\Column(name="title", type="string", length=512, nullable=false)
     */
    protected ?string $title = null;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $name
     *
     * @return self
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function __toString()
    {
        return $this->getTitle();
    }
}
