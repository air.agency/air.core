<?php
namespace Air\Core\Service\Serializer;

use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\Metadata\StaticPropertyMetadata;
use Air\Core\Service\Media\PathGenerator;

class SerializerHandler
{
    /** @var PathGenerator|null */
    private $pathGenerator = null;

    public function __construct(PathGenerator $pathGenerator)
    {
        $this->pathGenerator = $pathGenerator;
    }

    public function getPath(Media $media = null, $format = 'reference')
    {
        if (!$media) {
            return '';
        }

        $format = $this->provider->getFormatName($media, $format);

        return $this->provider->generatePublicUrl($media, $format);
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        /** @var MySpecialObjectType $object */
        $media = $event->getObject();

        $small = $this->pathGenerator->getPath($media, 'small');
        $medium = $this->pathGenerator->getPath($media, 'medium');
        $big = $this->pathGenerator->getPath($media, 'reference');
        $reference = $this->pathGenerator->getPath($media, 'reference');

        $key = 'customDataKey';
        $value = 'myvalue';

        $event->getVisitor()->visitProperty(
            new StaticPropertyMetadata('', "smallPath", $small),
            $small
        );

        $event->getVisitor()->visitProperty(
            new StaticPropertyMetadata('', "mediumPath", $medium),
            $medium
        );

        $event->getVisitor()->visitProperty(
            new StaticPropertyMetadata('', "bigPath", $big),
            $big
        );

        $event->getVisitor()->visitProperty(
            new StaticPropertyMetadata('', "originalPath", $reference),
            $reference
        );
    }
}
