<?php

namespace Air\Core\Entity\Media\Traits;

use App\Application\Sonata\MediaBundle\Entity\Media;
use JMS\Serializer\Annotation as Serializer;

trait LogoTrait
{
    /**
     * Logo
     *
     * @var Media
     *
     * @Serializer\Type("App\Application\Sonata\MediaBundle\Entity\Media")
     * @Serializer\Groups({"list", "view"})
     * @Serializer\MaxDepth(1)
     *
     * @ORM\ManyToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="logo", referencedColumnName="id")
     * })
     */
    protected $logo;

    /**
     * @param Media $media
     * @return $this
     */
    public function setLogo(?Media $media = null)
    {
        $this->logo = $media;

        return $this;
    }

    /**
     * @return Media|null
     */
    public function getLogo():?Media
    {
        return $this->logo;
    }
}
