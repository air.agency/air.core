<?php

namespace Air\Core\Entity;

use App\User\Entity\User;

interface AuthorAwareInterface
{
    /**
     * @return User|null
     */
    public function getCreatedBy(): ?User;

    /**
     * @param User|null $createdBy
     *
     * @return self
     */
    public function setCreatedBy(?User $createdBy);

    /**
     * @return User|null
     */
    public function getUpdatedBy(): ?User;

    /**
     * @param User|null $updatedBy
     *
     * @return self
     */
    public function setUpdatedBy(?User $updatedBy);
}
