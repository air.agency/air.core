<?php

namespace Air\Core\Entity\Jwt;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Air\Core\Entity\Traits;
use Gesdinet\JWTRefreshTokenBundle\Entity\AbstractRefreshToken;

/**
 * @ORM\Entity()
 * @ORM\Table(name="app_jwt_refresh_token")
 * @Gedmo\SoftDeleteable(fieldName="systemDeletedAt", timeAware=false)
 * @ORM\HasLifecycleCallbacks()
 */
class RefreshToken extends AbstractRefreshToken
{
    use Traits\IdTrait;
    use Traits\TimestampableTrait;

    /**
     * Id 
     *
     * @var integer
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({"list", "view", "id"})
     *
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Assert\Type("int")
     */
    protected $id;
}
