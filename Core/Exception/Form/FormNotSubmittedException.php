<?php

namespace Air\Core\Exception\Form;

use Air\Core\Exception\ClientException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class FormNotSubmittedException extends ClientException
{
    /**
     * FormNotSubmittedException constructor.
     *
     * @param Throwable|null $previous
     */
    public function __construct(Throwable $previous = null)
    {
        parent::__construct('Form is not submitted', Response::HTTP_BAD_REQUEST, $previous);
    }

}
