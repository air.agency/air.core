<?php

namespace Air\Core\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait PhoneTrait
{
    /**
     * Phone
     *
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list", "view"})
     *
     * @Assert\Length(max=32)
     *
     * @ORM\Column(name="phone", type="string", length=32, nullable=true)
     */
    protected ?string $phone = null;

    /**
     * @return null|string
     */
    public function getPhone():?string
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     * @return self
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }
}
