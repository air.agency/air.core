<?php
namespace Air\Core\Service\Serializer;

use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\JsonSerializationVisitor;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\CamelCaseNamingStrategy;
use JMS\Serializer\SerializerInterface;

class SerializerHelper
{
    protected SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /*
     * Object to array
     */
    public function toArray($data, array $groups = [])
    {
//        $builder = SerializerBuilder::create();
//        $builder->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy());
//        $serializer = $builder->build();
//
        $context = SerializationContext::create();
        $context->setGroups($groups);
        $context->setSerializeNull(true);

        return $this->serializer->toArray($data, $context);
    }

    /*
     * Object to json
     */
    public function toJson($data, array $groups = [])
    {
/*
        $serializebuilder = SerializerBuilder::create();
        $serializebuilder->setPropertyNamingStrategy(new \JMS\Serializer\Naming\IdenticalPropertyNamingStrategy());

        //$namingStrategy = new SerializedNameAnnotationStrategy(new CamelCaseNamingStrategy());
        //$jsonVisitor = new JsonSerializationVisitor($namingStrategy);
        //$jsonVisitor->setOptions(JSON_UNESCAPED_SLASHES);
        //$builder->addDefaultSerializationVisitors();
        //$builder->setSerializationVisitor('json', $jsonVisitor);
        $serializer = $serializebuilder->build();
*/

        $context = SerializationContext::create();
        $context->setGroups($groups);
        $context->setSerializeNull(true);

        return $this->serializer->serialize($data, 'json', $context);
    }

    /*
     * Json to object
     */
    public function toObject($data, string $toObject, array $groups = [])
    {
        //$serializebuilder = SerializerBuilder::create();
        //$serializebuilder->setPropertyNamingStrategy(new \JMS\Serializer\Naming\IdenticalPropertyNamingStrategy());
        //$serializer = $serializebuilder->build();
        
        $context = SerializationContext::create();
        $context->setGroups($groups);

        return $this->serializer->deserialize($data, $toObject, 'json');
    }
}
