<?php

namespace Air\Core\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait OrderingTrait
{
    /**
     * Ordering
     *
     * @var string
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({"list", "view"})
     *
     *
     * @ORM\Column(name="ordering", type="integer", nullable=true)
     */
    protected ?int $ordering = null;

    /**
     * @return null|int
     */
    public function getOrdering():?int
    {
        return $this->ordering;
    }

    /**
     * @param null|int $ordering
     * @return self
     */
    public function setOrdering(?int $ordering): self
    {
        $this->ordering = $ordering;
        return $this;
    }
}
