<?php

namespace Air\Core\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait UrlTrait
{
    /**
     * Url
     *
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Column(name="url", type="text",  nullable=true)
     */
    protected ?string $url = null;

    /**
     * @return null|string
     */
    public function getUrl():?string
    {
        return $this->url;
    }

    /**
     * @param null|string $url
     * @return self
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;
        return $this;
    }
}
