<?php

namespace Air\Core\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use \DateTime;

trait DateToTrait
{
    /**
     * Date to
     *
     * @Serializer\Type("datetime")
     * @Serializer\Groups({"list", "view"})
     *
     * @ORM\Column(name="date_to", type="datetime", nullable=true)
     */
    protected ?DateTime $dateTo = null;

    /**
     * @param DateTime $dateTo
     */
    public function setDateTo(?DateTime $dateTo)
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateTo():?DateTime
    {
        return $this->dateTo;
    }
}
