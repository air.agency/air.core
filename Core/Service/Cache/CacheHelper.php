<?php

namespace Air\Core\Service\Cache;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Tools\Pagination\Paginator;

class CacheHelper
{
    public static function generateCacheId(Request $request, array $params = [])
    {
        $getParams = $request->query->all();
        ksort($getParams);

        // JSON_NUMERIC_CHECK
        $cacheId = md5(json_encode($getParams, JSON_UNESCAPED_SLASHES | JSON_FORCE_OBJECT));

        $schemeAndHttpHost = $request->getSchemeAndHttpHost();
        if (isset($_ENV['HOST']) && isset($_ENV['SCHEME']) && $_ENV['HOST'] && $_ENV['SCHEME']) {
            $schemeAndHttpHost = $_ENV['SCHEME'] . "://" . $_ENV['HOST'];
        }
        //$cacheId = $request->getSchemeAndHttpHost().$request->getPathInfo()."_".$cacheId;
        $cacheId = $schemeAndHttpHost . $request->getPathInfo() . "_" . $cacheId;

        foreach ($params as $param) {
            $cacheId = $cacheId . "_" . $param;
        }

        $cacheId = str_replace("@", "_", $cacheId);
        $cacheId = str_replace(",", "_", $cacheId);
        $cacheId = str_replace(";", "_", $cacheId);
        $cacheId = str_replace(".", "_", $cacheId);
        $cacheId = str_replace(":", "_", $cacheId);
        $cacheId = str_replace("/", "_", $cacheId);
        
        return $cacheId;
    }

    public static function getTagByEntity($entity)
    {
        if (is_iterable($entity)) {
            return self::getTagByEntityList($entity);
        }

        if (is_object($entity)) {
            return self::normalizeTag(sprintf('%s-%s', get_class($entity), $entity->getId()));
        }

        return (string)self::normalizeTag($entity);
    }

    public static function getTagByEntityList($list)
    {
        $tags = [];
        foreach ($list as $item) {
            $tags[] = self::getTagByEntity($item);
        }
        return $tags;
    }

    public static function getTagByClass($class)
    {
        return self::normalizeTag($class);
    }

    public static function normalizeTag($tag)
    {
        return md5($tag);
    }
}
