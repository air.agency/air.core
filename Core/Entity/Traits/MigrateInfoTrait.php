<?php

namespace Air\Core\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

trait MigrateInfoTrait
{
    /**
     * Migrate info
     *
     * @var string|null
     *
     * @ORM\Column(name="migrate_info", type="text", nullable=true)
     */
    protected ?string $migrateInfo = null;

    /**
     * @return string|null
     */
    public function getMigrateInfo(): ?string
    {
        return $this->migrateInfo;
    }

    /**
     * @param string|null $migrateInfo
     *
     */
    public function setMigrateInfo(?string $migrateInfo)
    {
        $this->migrateInfo = $migrateInfo;

        return $this;
    }
}
